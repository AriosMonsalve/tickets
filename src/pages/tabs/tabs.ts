import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { TicketsPage } from "../tickets/tickets";
import { PerfilPage } from "../perfil/perfil";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = TicketsPage;
  tab3Root = PerfilPage;

  constructor() {

  }
}
